import https from "https";
import fs from "fs";
import http from "http";
import chalk from 'chalk';
import configuration from '../config';


const environment = process.env.NODE_ENV || 'development';
const config = configuration[environment];

export function serverResolver(app){
    const { ssl } = config;
    if (ssl) {
        try{
            return https.createServer(
                {
                    key: fs.readFileSync(`./ssl/${environment}/server.key`),
                    cert: fs.readFileSync(`./ssl/${environment}/server.crt`)
                },
                app
            )
        } catch (error) {
            console.log(chalk.greenBright.bgRed('something went wrong with ssl cert'));
            return null;
        }

    } else {
        return http.createServer(app)
    }
}
