import {ApolloServer,gql
} from "apollo-server-express"
import Express from 'express'
import 'dotenv/config'

import configuration from '../config';
import { serverResolver } from "./serverResolver";
import typeDefs from './graphql/schema';
import resolvers from './graphql/resolvers';
import UserAPI from "./graphql/data/UserAPI";
import StadiumAPI from "./graphql/data/StadiumAPI";

const environment = process.env.NODE_ENV || 'development';
const config = configuration[environment];

const apolloServer = new ApolloServer( { typeDefs, resolvers, dataSources: () => {
        return {
            userAPI: new UserAPI(),
            stadiumAPI: new StadiumAPI(),
        };
    }, } );
const app = Express();
apolloServer.applyMiddleware({ app });

const server = serverResolver(app);
if ( server ) {
    apolloServer.installSubscriptionHandlers(server);
    const { port } = config;
    server.listen({ port }, () => console.log(`Server ready at ${config.port}`));
}

