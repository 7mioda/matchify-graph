import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../../config';


const environment = process.env.NODE_ENV;
const configuration = config[environment];

export default class UserAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `${configuration.api}users`;
    }

    async getUsers() {
        return this.get('players');
    }

    async registerUser(email, password) {
        return this.post('register', {
            email,
            password
        });
    }

    async login(email, password) {
        return this.post('login', {
            email,
            password
        });
    }

}
