import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../../config';


const environment = process.env.NODE_ENV;
const configuration = config[environment];

export default class StadiumAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `${configuration.api}stadiums`;
    }

    async getStadiums() {
        return this.get('/');
    }

    async addStadium(stadium) {
        return this.post('/add-stadium', stadium);
    }

}
