import {merge} from 'lodash';
import Stadium from "./Stadium";
import User from "./User";


export default merge(
    [
        Stadium,
        User
    ]
)
