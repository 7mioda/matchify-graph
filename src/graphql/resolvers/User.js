export default {
    Query: {
        async Users(obj, args, { dataSources }, info ) {
            return await dataSources.userAPI.getUsers();
        }
    },
    Mutation: {
        async registerUser(obj, { userInput: { email, password } }, { dataSources }, info ) {
            return await dataSources.userAPI.registerUser(email, password);
        },
        async login(obj, { userInput: { email, password } }, { dataSources }, info ) {
            return await dataSources.userAPI.login(email, password);
        }
    }
}
