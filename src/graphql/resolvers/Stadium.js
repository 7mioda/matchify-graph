export default {
    Query: {
        async Stadiums(obj, args, { dataSources }, info) {
            return await dataSources.stadiumAPI.getStadiums();
        },
        Stadium(obj, args, context, info ) {
            return {}
        }
    },
    Mutation: {
        async addStadium(obj, { stadium: stadiumInput }, { dataSources }, info ) {
            const { stadium } = await dataSources.stadiumAPI.addStadium(stadiumInput);
            return stadium;
        }
    }
}
