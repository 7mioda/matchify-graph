import { gql } from 'apollo-server-express';


export default gql`
    input PricingInput {
        type: String
        price: Float
    }
    type Pricing {
        type: String
        price: Float
    }
`;
