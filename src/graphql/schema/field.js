import { gql } from 'apollo-server-express';

export default gql`
    input DimensionInput {
        width: Float
        length: Float
    }
    
    input FieldInput {
        dimensions: DimensionInput
        pricing: PricingInput
        photos: [String]
        updatedAt: String
        createdAt: String
    }
    type Dimension {
        width: Float
        length: Float
    }
    
    type Field {
        dimensions: Dimension
        pricing: Pricing
        photos: [String]
        updatedAt: String
        createdAt: String
    }

`;
