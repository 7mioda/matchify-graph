import { gql } from 'apollo-server-express';


export default gql`
    
    type Token {
        token: String
        refreshToken: String
    }
    
    input UserInput {
        email: String
        password: String
    }
    
    type User {
        _id: ID
        email: String
        password: String
        firstConnection: String
        updatedAt: String
        createdAt: String
    }
    
    extend type Query {
        Users : [User]
        UserById : User
    }
    
    extend type Mutation {
        registerUser (userInput: UserInput): Token
        login (userInput: UserInput): Token
    }
`;
