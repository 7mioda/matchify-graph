import { gql } from 'apollo-server-express';

export default gql`
    type Fault {
        match: Match
        referee: User
        playerA: User
        playerB: User
        card: String
        description: String
        createdAt: Date
    }
`;
