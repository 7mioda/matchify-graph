import { gql } from 'apollo-server-express';


export default gql`
    input LocationInput {
        type: String
        coordinates: [Float]
    }
    type Location {
        type: String
        coordinates: [Float]
    }
`;
