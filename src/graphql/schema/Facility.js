import { gql } from 'apollo-server-express';

export default gql`
    input FacilityInput {
        name: String
        description: String
        pricing: String
        photos: [String]
    }
    
    type Facility {
        name: String
        description: String
        pricing: String
        photos: [String]
    }
`;
