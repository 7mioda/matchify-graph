import { gql } from 'apollo-server-express';

export default gql`    
    # This is a comment
    input StadiumInput {
        owner: ID
        # location should be input not string
        location: LocationInput
        facilities: [FacilityInput]
        fields: [FieldInput]
        photos: [Upload],
        updatedAt: String
        default: String
        createdAt: String
    }
    type Stadium {
        id: ID
        owner: User
        location: Location
        facilities: [Facility]
        fields: [Field]
        photos: [String],
        updatedAt: String
        createdAt: String
    }
    
    extend type Query {
        Stadiums: [Stadium]
        Stadium(stadiumId: ID): Stadium
    }
    
    extend type Mutation {
        addStadium(stadium: StadiumInput): Stadium
    }
`;
