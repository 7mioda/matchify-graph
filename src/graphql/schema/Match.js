import { gql } from 'apollo-server-express';

export default gql`
    
    input MatchInput {
        teams: [ID],
        referee: [ID]
        stadium: ID,
        duration: Int
        status: String
        date: Date
        updatedAt: Date
        createdAt: Date
    }
    
    type Match {
        teams: [Team],
        referee: [User]
        stadium: Stadium,
        duration: Int
        status: String
        goals: [Goal],
        date: Date
        score: [Score],
        stats: MatchStat,
        updatedAt: Date
        createdAt: Date
    }
    
    extend type Query {
        Match(matchId: ID): Match
        Matches: [Match]
    }

    
    extend type Mutation {
        addMatch(matchInput: MatchInput): Match
    }
`;
