import { gql } from 'apollo-server-express';

export default gql`
    type Penalty {
        match: Match
        referee: User
        description: String,
        fault: Fault
        goal: Goal
        createdAt: Date
    }
`;
