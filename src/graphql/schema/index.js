import { gql } from 'apollo-server-express';
import Facility from "./Facility";
import Field from './field';
import User from "./User";
import Location from "./location";
import Pricing from "./Pricing";
import Stadium from "./stadium";
import Team from "./Team";
import Match from "./Match";
import Goal from "./Goal";
import MatchStat from "./MatchStat";
import Fault from "./Fault";
import Score from "./Score";
import Penalty from "./Penalty";

const root = gql`
    scalar Date
    
    type Query {
        root: String
    }
    
    type Mutation {
        root:String
    }
`;


export default [
    root,
    Facility,
    Pricing,
    Field,
    User,
    Location,
    Stadium,
    Team,
    Match,
    Goal,
    MatchStat,
    Penalty,
    Fault,
    Score
]
