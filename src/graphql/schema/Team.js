import { gql } from 'apollo-server-express';


export default gql`    
    input TeamInput {
        captain: ID
        members: [ID]
        matches: [ID]
        logo: String
        updatedAt: String
        createdAt: String
    }
    
    type Team {
        captain: User
        members: [User]
        matches: [String]
        logo: String
        updatedAt: String
        createdAt: String
    }
    
    extend type Query {
        Team(teamId: ID): Team
        Teams: [Team]
    }
    
    extend type Mutation {
        addTeam(teamInput: TeamInput): Team
    }
`;
