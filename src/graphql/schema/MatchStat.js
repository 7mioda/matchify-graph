import { gql } from 'apollo-server-express';

export default gql`
    input MatchStatInput {
        topScorer: ID
        topDefender: ID
        topPlayer: ID
        penalties: [ID]
        faults: [ID]
        goals: [ID]
        score: [ID]
    }
    
    type MatchStat {
        topScorer: User
        topDefender: User
        topPlayer: User
        penalties: [Penalty]
        faults: [Fault]
        goals: [Goal]
        score: [Score]
    }
`;
