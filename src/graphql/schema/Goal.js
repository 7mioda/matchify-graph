import { gql } from 'apollo-server-express';

export default gql`
    input GoalInput {
        referee: ID
        player: ID
        gradient: ID
        description: String
        createdAt: Date
        default: Date
    }
    
    type Goal {
        match: Match
        referee: User
        player: User
        gradient: User
        description: String
        createdAt: Date
        default: Date
    }
`;
